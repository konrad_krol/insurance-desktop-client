
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllCarModels_QNAME = new QName("http://webservice/", "getAllCarModels");
    private final static QName _CreateCarModelResponse_QNAME = new QName("http://webservice/", "createCarModelResponse");
    private final static QName _GetAllCarModelsResponse_QNAME = new QName("http://webservice/", "getAllCarModelsResponse");
    private final static QName _DeleteCarModelResponse_QNAME = new QName("http://webservice/", "deleteCarModelResponse");
    private final static QName _CreateCarModel_QNAME = new QName("http://webservice/", "createCarModel");
    private final static QName _DeleteCarModel_QNAME = new QName("http://webservice/", "deleteCarModel");
    private final static QName _UpdateCarModelResponse_QNAME = new QName("http://webservice/", "updateCarModelResponse");
    private final static QName _UpdateCarModel_QNAME = new QName("http://webservice/", "updateCarModel");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteCarModelResponse }
     * 
     */
    public DeleteCarModelResponse createDeleteCarModelResponse() {
        return new DeleteCarModelResponse();
    }

    /**
     * Create an instance of {@link UpdateCarModelResponse }
     * 
     */
    public UpdateCarModelResponse createUpdateCarModelResponse() {
        return new UpdateCarModelResponse();
    }

    /**
     * Create an instance of {@link DeleteCarModel }
     * 
     */
    public DeleteCarModel createDeleteCarModel() {
        return new DeleteCarModel();
    }

    /**
     * Create an instance of {@link CarModel }
     * 
     */
    public CarModel createCarModel() {
        return new CarModel();
    }

    /**
     * Create an instance of {@link CreateCarModelResponse }
     * 
     */
    public CreateCarModelResponse createCreateCarModelResponse() {
        return new CreateCarModelResponse();
    }

    /**
     * Create an instance of {@link GetAllCarModels }
     * 
     */
    public GetAllCarModels createGetAllCarModels() {
        return new GetAllCarModels();
    }

    /**
     * Create an instance of {@link GetAllCarModelsResponse }
     * 
     */
    public GetAllCarModelsResponse createGetAllCarModelsResponse() {
        return new GetAllCarModelsResponse();
    }

    /**
     * Create an instance of {@link CreateCarModel }
     * 
     */
    public CreateCarModel createCreateCarModel() {
        return new CreateCarModel();
    }

    /**
     * Create an instance of {@link UpdateCarModel }
     * 
     */
    public UpdateCarModel createUpdateCarModel() {
        return new UpdateCarModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllCarModels }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getAllCarModels")
    public JAXBElement<GetAllCarModels> createGetAllCarModels(GetAllCarModels value) {
        return new JAXBElement<GetAllCarModels>(_GetAllCarModels_QNAME, GetAllCarModels.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCarModelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "createCarModelResponse")
    public JAXBElement<CreateCarModelResponse> createCreateCarModelResponse(CreateCarModelResponse value) {
        return new JAXBElement<CreateCarModelResponse>(_CreateCarModelResponse_QNAME, CreateCarModelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllCarModelsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getAllCarModelsResponse")
    public JAXBElement<GetAllCarModelsResponse> createGetAllCarModelsResponse(GetAllCarModelsResponse value) {
        return new JAXBElement<GetAllCarModelsResponse>(_GetAllCarModelsResponse_QNAME, GetAllCarModelsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCarModelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "deleteCarModelResponse")
    public JAXBElement<DeleteCarModelResponse> createDeleteCarModelResponse(DeleteCarModelResponse value) {
        return new JAXBElement<DeleteCarModelResponse>(_DeleteCarModelResponse_QNAME, DeleteCarModelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCarModel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "createCarModel")
    public JAXBElement<CreateCarModel> createCreateCarModel(CreateCarModel value) {
        return new JAXBElement<CreateCarModel>(_CreateCarModel_QNAME, CreateCarModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCarModel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "deleteCarModel")
    public JAXBElement<DeleteCarModel> createDeleteCarModel(DeleteCarModel value) {
        return new JAXBElement<DeleteCarModel>(_DeleteCarModel_QNAME, DeleteCarModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCarModelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updateCarModelResponse")
    public JAXBElement<UpdateCarModelResponse> createUpdateCarModelResponse(UpdateCarModelResponse value) {
        return new JAXBElement<UpdateCarModelResponse>(_UpdateCarModelResponse_QNAME, UpdateCarModelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCarModel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updateCarModel")
    public JAXBElement<UpdateCarModel> createUpdateCarModel(UpdateCarModel value) {
        return new JAXBElement<UpdateCarModel>(_UpdateCarModel_QNAME, UpdateCarModel.class, null, value);
    }

}
