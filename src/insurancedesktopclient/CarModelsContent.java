/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package insurancedesktopclient;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import webservice.AuthenticationWebService;
import webservice.CarModel;
import webservice.CarModelsWebService;

/**
 *
 * @author konrad
 */
public class CarModelsContent {

    MainAdminWindow mainWindow;
    
    CarModelsContent(MainAdminWindow mainWindow) {
        this.mainWindow = mainWindow;
    }
    
    public void refresh() {
        CarModelsWebService webservicePort = mainWindow.getWebservicePort();
        List<CarModel> carModels = webservicePort.getAllCarModels();
        
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Nazwa");
        tableModel.addColumn("Rok produkcji");
        tableModel.addColumn("Cena");
        
        ListIterator<CarModel> iterator = carModels.listIterator();
        while(iterator.hasNext()) {
            CarModel carModel = iterator.next();
            
            ArrayList<String> row = new ArrayList<String>();
            row.add(carModel.getId().toString());
            row.add(carModel.getName().toString());
            row.add(carModel.getManufactureYear().toString());
            row.add(carModel.getPrice().toString());
            
            tableModel.addRow(row.toArray());
        }
            
        
        mainWindow.getjTable1().setModel(tableModel);
    }

    void update(Integer id) {
        CarModelsWebService webservicePort = mainWindow.getWebservicePort();
        CarModel carModel = new CarModel();
        
        TableModel tableModel = mainWindow.getjTable1().getModel();
        carModel.setId(Long.valueOf((String) tableModel.getValueAt(id, 0)));
        carModel.setName((String) tableModel.getValueAt(id, 1));
        carModel.setManufactureYear(Integer.valueOf((String) tableModel.getValueAt(id, 2)));
        carModel.setPrice(Float.valueOf((String) tableModel.getValueAt(id, 3)));
        
        webservicePort.updateCarModel(carModel.getId().intValue(), carModel.getManufactureYear(), carModel.getName(), carModel.getPrice());
    }

    void remove(Integer tableId) {
        TableModel tableModel = mainWindow.getjTable1().getModel();
        Long id = Long.valueOf((String) tableModel.getValueAt(tableId, 0));
        
        CarModelsWebService webservicePort = mainWindow.getWebservicePort();
        webservicePort.deleteCarModel(id.intValue());
        
        this.refresh();
    }

    void create(String name, Integer manufactureYear, Float price) {
        CarModelsWebService webservicePort = mainWindow.getWebservicePort();
        webservicePort.createCarModel(manufactureYear, name, price);
    }

    void authenticate(String username, String password) {
        AuthenticationWebService webserviceClient = mainWindow.getAuthWebservicePort();
        Boolean status = webserviceClient.authenticate(username, password);
        if(status) {
            mainWindow.setCurrentUsername(username);
            mainWindow.getjLabel2().setText(username);
        }
        else {
            mainWindow.setCurrentUsername(null);
            mainWindow.getjLabel2().setText("");
        }
    }
    
}
